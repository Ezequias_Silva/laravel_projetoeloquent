<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Listagem de Editoras</title>
    <link rel="stylesheet" type="text/css" href="/css/app.css">
  </head>
  <body>

    <div class="container">

      <div class="pager-header">
        <h2>Listagem de Editoras</h2>
      </div>

      <div class="row">

        <div class="col-md-6">

          <div class="panel panel-primary">

            <div class="panel-heading">Editoras</div>

            <table class="table table-striped">

              <thead>
                <tr>
                  <th>Id</th>
                  <th>Nome</th>
                  <th>Email</th>
                  <th>Detalhes</th>
                </tr>
              </thead>

              <tbody>
                @foreach ($editoras as $editora)
                  <tr>
                    <td>{{ $editora->id }}</td>
                    <td>{{ $editora->nome }}</td>
                    <td>{{ $editora->email }}</td>
                    <td>
                      <a href="/editoras/{{ $editora->id }}">Detalhes</a>
                    </td>
                  </tr>
                @endforeach
              </tbody>

            </table>



          </div>

        </div>

      </div>

    </div>
    <script type="text/javascript" src="/js/app.js">

    </script>
  </body>
</html>
